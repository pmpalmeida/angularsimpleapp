# eInvoice Web Application

## Table of contents

1. [Prerequisites](#markdown-header-prerequisites)
2. [Installing](#markdown-header-installing)
3. [Running the Application](#markdown-header-running-the-application)
4. [i18n](#markdown-header-i18n)

## Prerequisites

Before you begin, make sure your development environment includes [Node.js®](https://nodejs.org/) and an npm package manager (should come with Node.js already).

### Node.js

Angular requires Node.js version 10.9.0 or later.

* Run `node -v` in a terminal to check your version.

### Angular CLI

We will use the Angular CLI to create projects, generate application and library code, and perform a variety of ongoing development tasks such as testing, bundling, and deployment.

Install the Angular CLI globally using `npm`.

```
npm install -g @angular/cli
```

## Installing

Before anything else, after you've cloned the repository you need to fetch all the npm dependencies.

To do this:

1. Go to the workspace folder.
2. Execute the command `npm install`.

```
cd <workspace-location>
npm install
```

This will fetch all the npm dependencies that the project requires. This is only required when you clone the project.

## Running the application

The Angular CLI includes a server for easy building and serving of the app locally.

1. Go to the workspace folder.
2. Launch the server by using the CLI command `ng server`.

```
cd <workspace-location>
ng serve --open
```

**Note:** Alternatively you can use the command `npm start`.

The `ng serve` command launches the server, watches your files, and rebuilds the app as you make changes to those files.

The `--open` option automatically opens your browser to `http://localhost:4200`.

## i18n

Angular provides internationalization tools. For more info on these check out their [website](https://angular.io/guide/i18n).

To perform internationalization in this project you need to follow these steps:

1. Run the provided script `prep_i18n.sh`. This will create every file needed in `src/locale/`.
2. Each of the generated files corresponds to a different translation file (one per language). Edit these in the [following manner](https://angular.io/guide/i18n#translate-text-nodes).
3. Run the command `npm run build-i18n` in the project directory.

These steps will build a static application for each language in `/dist/`.

To extend the languages that the application provides simply:

* Edit the `prep_i18n.sh` file by adding the new language prefix (e.g., pt, en, etc) in the `langs` array.
* Create a new script in `package.json` to mimic the `build-i18n:es` and `build-i18n:pt`.
* Edit the `build-i18n` script in `package.json` by adding the newly created script.
* Proceed from step one.

**WARNING:** Executing the script `prep_i18n.sh` will **DELETE** all the previous progress. Be sure to back them up before executing.