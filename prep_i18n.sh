langs=("pt" "en")

echo Starting xi18n...

ng xi18n --output-path src/locale

echo Creating language specific files.

for lang in "${langs[@]}"
do
	cp src/locale/messages.xlf src/locale/messages.$lang.xlf
done

rm src/locale/messages.xlf

echo Done.