import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ApiService } from './services/api.service';
import { Logger } from './services/logger.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { CoreRoutingModule } from './core-routing.module';
import { HomeComponent } from './home/home.component';
import { EnrollmentModule } from '../pages/enrollment/enrollment.module';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    EnrollmentModule
  ],
  declarations: [
    NotFoundComponent,
    HomeComponent
  ],
  exports: [
    RouterModule
  ],
  providers: [
    ApiService,
    Logger
  ]
})
export class CoreModule { }
