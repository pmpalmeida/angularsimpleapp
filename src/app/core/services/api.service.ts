import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const baseURL = "http://localhost:8080/pokedex";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(url?: string) {
    let path = this.treatUrl(url);
    
    this.doGet(path)
      .subscribe((data: any) => {
        return data;
      });
  }

  post(payload: any, url?: string) {
    let path = this.treatUrl(url);

    this.doPost(payload, path)
      .subscribe((data: any) => {
        return data;
      });
  }

  put(payload: any, url?: string) {
    let path = this.treatUrl(url);

    this.doPut(payload, path)
      .subscribe((data: any) => {
        return data;
      });
  }

  delete(url?: string) {
    let path = this.treatUrl(url);

    this.doDelete(path)
      .subscribe((data: any) => {
        return data;
      });
  }

  private doGet(path: string) {
    return this.http.get<string>(baseURL + path);
  }

  private doPost(path: string, payload: any) {
    return this.http.post<any>(baseURL + path, payload);
  }

  private doPut(path: string, payload: any) {
    return this.http.put<any>(baseURL + path, payload);
  }

  private doDelete(path: string) {
    return this.http.delete<any>(baseURL + path);
  }

  //utils
  private treatUrl(url?: string) {
    if(url) {
      return url;
    } else {
      return "";
    }
  }
}