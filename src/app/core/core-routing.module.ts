import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { PartnerEnrollmentComponent } from '../pages/enrollment/partner/partner.component';
import { IssuerEnrollmentComponent } from '../pages/enrollment/issuer/issuer.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent
    },
    {
        path: 'signup/partner',
        pathMatch: 'full',
        component: PartnerEnrollmentComponent
    },
    {
        path: 'signup/issuer',
        pathMatch: 'full',
        component: IssuerEnrollmentComponent
    },
    {
        path: '**',
        component: NotFoundComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class CoreRoutingModule { }