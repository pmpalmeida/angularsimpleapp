import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-partner-enrollment',
  templateUrl: './partner.component.html',
  styleUrls: [
    './partner.component.css',
    '../../../shared/styles/layout.css'
  ]
})
export class PartnerEnrollmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
