import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { MdbModule } from '../../shared/mdb.module';

import { PartnerEnrollmentComponent } from './partner/partner.component';
import { IssuerEnrollmentComponent } from './issuer/issuer.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        MdbModule
    ],
    declarations: [
        PartnerEnrollmentComponent,
        IssuerEnrollmentComponent
    ]
})
export class EnrollmentModule { }