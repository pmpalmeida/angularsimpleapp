import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-issuer-enrollment',
  templateUrl: './issuer.component.html',
  styleUrls: [
    './issuer.component.css',
    '../../../shared/styles/layout.css'
  ]
})
export class IssuerEnrollmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
