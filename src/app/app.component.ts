import { Component } from '@angular/core';
import { Logger } from './core/services/logger.service';
import { ApiService } from './core/services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private logger: Logger,
    private api: ApiService
  ) {}
  title = 'angularsimpleapp';

  get() {
    this.api.get();
  }
}
